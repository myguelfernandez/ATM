/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.sd.atm;

import com.google.gson.Gson;
import java.net.URL;
import py.sd.corebanco.CoreBancoWs_Service;
import py.sd.corebanco.LoginRespuesta;
import py.sd.corebanco.CoreBancoWs;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import py.sd.corebanco.BaseRespuesta;
/**
 *
 * @author Miguel Fernandez <myguelfernandez@gmail.com>
 */
public class CoreBancoConsume {
    
    
    private static final Logger logger = LogManager.getLogger(CoreBancoConsume.class);
    
    public static LoginRespuesta login(String user, String pass){
        LoginRespuesta resp = new LoginRespuesta();
        Gson gson = new Gson();
        logger.info("IN: [{}]",user,pass);
        try { 
            
            CoreBancoWs_Service service = new CoreBancoWs_Service(new URL(Constantes.URL_WS));
            CoreBancoWs port = service.getCoreBancoWsPort();

            resp  = port.login(user, pass);
        } catch (Exception ex) {
            logger.error(ex.getMessage(),ex);
            resp.setEstado(-1);
            resp.setMensaje(ex.getMessage());
        }
        logger.info("OUT: "+gson.toJson(resp));
        return resp;
    }
    
    public static BaseRespuesta deposito(String token, String cuenta, Double monto){
        logger.info("IN: [{}]",token,cuenta,monto);
        BaseRespuesta resp = new BaseRespuesta();
        Gson gson = new Gson();
        try {
            
            CoreBancoWs_Service service = new CoreBancoWs_Service(new URL(Constantes.URL_WS));
            CoreBancoWs port = service.getCoreBancoWsPort();
            
            resp = port.deposito(cuenta, monto, token);
        } catch (Exception e) {
            logger.error(e.getMessage(),e);
            resp.setEstado(-1);
            resp.setMensaje(e.getMessage());
        }
        logger.info("OUT: "+gson.toJson(resp));
        return resp;
    }
    
    public static BaseRespuesta extraccion(String token, String cuenta, Double monto){
        logger.info("IN: [{}]",token,cuenta,monto);
        Gson gson = new Gson();
        BaseRespuesta resp = new BaseRespuesta();
        try {
            
            CoreBancoWs_Service service = new CoreBancoWs_Service(new URL(Constantes.URL_WS));
            CoreBancoWs port = service.getCoreBancoWsPort();
            
            resp = port.extraccion(cuenta.toString(), monto, token);
        } catch (Exception e) {
            logger.error(e.getMessage(),e);
            resp.setEstado(-1);
            resp.setMensaje(e.getMessage());
        }
        logger.info("OUT: "+gson.toJson(resp));
        return resp;
    }
    
    public static BaseRespuesta transferencia(String token, Integer cuentaOringen,Integer cuentaDestino, Double monto){
        logger.info("IN: [{}]",token,cuentaOringen,cuentaDestino,monto);
        BaseRespuesta resp = new BaseRespuesta();
        Gson gson = new Gson();
        try {
            
            CoreBancoWs_Service service = new CoreBancoWs_Service(new URL(Constantes.URL_WS));
            CoreBancoWs port = service.getCoreBancoWsPort();
            
            resp = port.transferencia(cuentaOringen, cuentaDestino, monto, token);
        } catch (Exception e) {
            logger.error(e.getMessage(),e);
            resp.setEstado(-1);
            resp.setMensaje(e.getMessage());
        }
        logger.info("OUT: "+gson.toJson(resp));
        return resp;
    }
    
    public static BaseRespuesta transferenciaMoneda(String token, Integer cuentaOringen,Integer cuentaDestino, Double monto,String tipoMoneda){
        logger.info("IN: [{}]",token,cuentaOringen,cuentaDestino,monto);
        BaseRespuesta resp = new BaseRespuesta();
        Gson gson = new Gson();
        try {
            
            CoreBancoWs_Service service = new CoreBancoWs_Service(new URL(Constantes.URL_WS));
            CoreBancoWs port = service.getCoreBancoWsPort();
            
            resp = port.transferenciaMoneda(cuentaOringen, cuentaDestino, monto, tipoMoneda, token);
        } catch (Exception e) {
            logger.error(e.getMessage(),e);
            resp.setEstado(-1);
            resp.setMensaje(e.getMessage());
        }
        logger.info("OUT: "+gson.toJson(resp));
        return resp;
    }
}
