/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.sd.atm;

import com.google.gson.Gson;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;
import java.util.Scanner;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.config.ConfigurationSource;
import org.apache.logging.log4j.core.config.Configurator;
import py.sd.corebanco.BaseRespuesta;
import py.sd.corebanco.LoginRespuesta;

/**
 *
 * @author Miguel Fernandez <myguelfernandez@gmail.com>
 */
public class Main {

    private static final Logger logger = LogManager.getLogger(CoreBancoConsume.class);

    public static void main(String[] args) {
        Gson gson = new Gson();
        Scanner scanner = new Scanner(System.in);
        LoginRespuesta loginRespuesta = new LoginRespuesta();

        try {
            InputStream log4j2 = Main.class.getClassLoader().getResourceAsStream("log4j2.xml");
            System.out.println("log4j2: " + log4j2);
            ConfigurationSource source = new ConfigurationSource(log4j2);
            Configurator.initialize(null, source);

            Properties properties = new Properties();
            FileInputStream fileInput;
            fileInput = new FileInputStream(Constantes.FILE_CONFIG_PATH);
            properties.load(fileInput);
            Constantes.URL_WS = properties.getProperty("url.core");
            Constantes.USER = properties.getProperty("user");
            Constantes.PASSWORD = properties.getProperty("pass");
        } catch (Exception e) {
            System.out.println("ERROR");
            e.printStackTrace();
        }

        try {
            System.out.println("Seleccione operacion :");
            System.out.println("1 => Deposito");
            System.out.println("2 => Extraccion");
            System.out.println("3 => Transferencia");
            System.out.println("4 => TransferenciaMoneda");
            String operacion = scanner.nextLine();
            switch (operacion) {
                case "1":
                    loginRespuesta = CoreBancoConsume.login(Constantes.USER, Constantes.PASSWORD);
                    if (loginRespuesta.getEstado() == 0) {
                        System.out.println("Ingrese cuenta");
                        String cuenta = scanner.nextLine();
                        System.out.println("Ingrese monto:");
                        Double monto = scanner.nextDouble();
                        BaseRespuesta depositoRespuesta = CoreBancoConsume.deposito(loginRespuesta.getToken(), cuenta, monto);
                        System.out.println(gson.toJson(depositoRespuesta));
                    } else {
                        System.out.println("fallo al establecer sesion");
                    }
                    break;
                case "2":
                    loginRespuesta = CoreBancoConsume.login(Constantes.USER, Constantes.PASSWORD);
                    if (loginRespuesta.getEstado() == 0) {
                        System.out.println("Ingrese cuenta");
                        String cuenta = scanner.nextLine();
                        System.out.println("Ingrese monto:");
                        Double monto = scanner.nextDouble();
                        BaseRespuesta depositoRespuesta = CoreBancoConsume.extraccion(loginRespuesta.getToken(), cuenta, monto);
                        System.out.println(gson.toJson(depositoRespuesta));
                    } else {
                        System.out.println("fallo al establecer sesion");
                    }
                    break;
                case "3":
                    loginRespuesta = CoreBancoConsume.login(Constantes.USER, Constantes.PASSWORD);
                    if (loginRespuesta.getEstado() == 0) {
                        System.out.println("Ingrese cuenta Origen");
                        Integer cuentaOrigen = scanner.nextInt();
                        System.out.println("Ingrese cuenta Destino");
                        Integer cuentaDestino = scanner.nextInt();
                        System.out.println("Ingrese monto:");
                        Double monto = scanner.nextDouble();
                        BaseRespuesta depositoRespuesta = CoreBancoConsume.transferencia(loginRespuesta.getToken(), cuentaOrigen, cuentaDestino, monto);
                        System.out.println(gson.toJson(depositoRespuesta));
                    } else {
                        System.out.println("fallo al establecer sesion");
                    }
                    break;
                case "4":
                    loginRespuesta = CoreBancoConsume.login(Constantes.USER, Constantes.PASSWORD);
                    if (loginRespuesta.getEstado() == 0) {
                        System.out.println("Ingrese cuenta Origen");
                        Integer cuentaOrigen = scanner.nextInt();
                        System.out.println("Ingrese cuenta Destino");
                        Integer cuentaDestino = scanner.nextInt();
                        System.out.println("Ingrese tipo de moneda");
                        System.out.println("1. Dolar");
                        System.out.println("2. Peso Argentino");
                        Integer tipoMoneda = scanner.nextInt();
                        System.out.println("Ingrese monto:");
                        Double monto = scanner.nextDouble();
                        BaseRespuesta depositoRespuesta = CoreBancoConsume.transferenciaMoneda(loginRespuesta.getToken(), cuentaOrigen, cuentaDestino, monto, tipoMoneda.toString());
                        System.out.println(gson.toJson(depositoRespuesta));
                    } else {
                        System.out.println("fallo al establecer sesion");
                    }
                    break;
                default:
                    System.out.println("Opcion no validad");
                    break;
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }

    }
}
